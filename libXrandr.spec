Name:           libXrandr
Version:        1.5.4
Release:        1
License:        MIT
Summary:        X.Org X11 libXrandr runtime library
URL:            https://www.x.org
Source0:        https://xorg.freedesktop.org/archive/individual/lib/%{name}-%{version}.tar.gz

BuildRequires:  xorg-x11-util-macros autoconf automake libtool
BuildRequires:  xorg-x11-proto-devel libX11-devel libXext-devel libXrender-devel

Requires:       libX11 >= 1.6.0

%description
X.Org X11 libXrandr runtime library

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}

%description    devel
The %{name}-devel package contains libraries and header files for %{name}.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
autoreconf -ivf
%configure --disable-static
%make_build

%install
%make_install
%delete_la

%ldconfig_scriptlets

%files
%defattr(-,root,root)
%doc AUTHORS 
%license COPYING
%{_libdir}/*.so.*

%files          devel
%defattr(-,root,root)
%{_includedir}/X11/extensions/*.h
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc

%files          help
%defattr(-,root,root)
%{_mandir}/man3/*.3*

%changelog
* Wed Dec 27 2023 konglidong <konglidong@uniontech.com> - 1.5.4-1
- update version to 1.5.4

* Sat Feb 04 2023 zhouwenpei <zhouwenpei1@h-partners.com> - 1.5.3-1
- update to 1.5.3

* Fri Nov 25 2022 zhouwenpei <zhouwenpei1@h-partners.com> - 1.5.2-4
- update Correct tar file

* Fri Nov 04 2022 wangkerong <wangkerong@h-partners.com> - 1.5.2-3
- disable static library

* Wed Oct 26 2022 zhouwenpei <zhouwenpei1@h-partners.com> - 1.5.2-2
- Rebuild for next release

* Fri Jul 24 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.5.2-1
- updata package

* Sat Oct 19 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.5.1-10
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:change the directory of the license file

* Tue Sep 10 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.5.1-9
- Package init
